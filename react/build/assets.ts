import fs from 'fs'
import path from 'path'
import hasha from 'hasha'

const listFiles = (dirPath: string, prefix: string, files: Array<string>) => {
  fs.readdirSync(dirPath).forEach(file => {
    const name = prefix === '' ? file : `${prefix}/${file}`

    if (fs.statSync(`${dirPath}/${file}`).isDirectory()) {
      listFiles(`${dirPath}/${file}`, name, files)
    } else {
      files.push(name)
    }
  })

  return files
}

const publicFiles = () => listFiles('./public', '', [])
const publicExcludes = ['robots.txt', 'sitemap.xml', 'favicon.ico']

export const publicAssets = publicFiles().map(src => {
  if (publicExcludes.includes(src)) {
    // Not statically referenced
    return { src, dest: src }
  } else {
    const ext = path.extname(src)
    const h = hasha.fromFileSync(`public/${src}`, { algorithm: 'md5' })
    const dest = src.substring(0, src.length - ext.length) + '.' + h + ext

    return { src, h, dest }
  }
})

type HashedFile = {
  src: string
  h: string
}

export const hashedPublic = (() => {
  const acc: Array<HashedFile> = []

  publicAssets.forEach(({ src, h }) => {
    if (h) acc.push({ src, h })
  })

  return acc
})()

export const generateHashedFileMap = (files: Array<HashedFile>) => {
  const data = Object.fromEntries(
    files.map(({ src, h }) => {
      // To avoid having a place with all these asset filenames in clear
      // (that's public anyway, but prevent crawler)
      const key = stringHashCode(src)

      return [key, h]
    })
  )

  return `/* Secure hash mapping (no clear file path to avoid crawlers) */

// Keys are expected to be file path relative to 'public',
// hashed using stringHashCode below.
const data: { [key: string]: string } = ${JSON.stringify(data)}

function stringHashCode(s: string): string {
  let h = 0

  for (let i = 0; i < s.length; i++) {
    h = (Math.imul(31, h) + s.charCodeAt(i)) | 0
  }

  return h.toString()
}

export function fileHash(filePath: string): string | undefined {
  const k = stringHashCode(filePath)
  return data[k]
}`
}

const stringHashCode = (s: string) => {
  var h = 0

  for (let i = 0; i < s.length; i++) {
    h = (Math.imul(31, h) + s.charCodeAt(i)) | 0
  }

  return h
}
