import { intersection, object, string, boolean, Ok, Err } from 'idonttrustlikethat'
import { StatusType } from '@datadog/browser-logs'

const url = string.and(str => {
  try {
    new URL(str)
    return Ok(str)
  } catch (err) {
    return Err(`${str} is not a URL`)
  }
})

export const baseValidator = object({
  IS_PROD: boolean.optional().default(false),
  APP_ENV: string, // environment name (e.g. TST, STG, PRD)
  BACKEND_URL: url
}).map(o => ({
  isProd: o.IS_PROD,
  env: o.APP_ENV,
  backendUrl: o.BACKEND_URL
}))

const datadogLevelValidator = string.map(s => {
  switch (s) {
    case 'debug':
      return StatusType.debug
    case 'warn':
      return StatusType.warn
    case 'error':
      return StatusType.error
    default:
      return StatusType.info
  }
})

/**
 * If (client) token is specified, then Datadog logging is enabled;
 * See:
 * - https://docs.datadoghq.com/logs/log_collection/javascript/
 * - https://docs.datadoghq.com/account_management/api-app-keys/
 *
 * If moreover application ID is given, then Datadog RUM is enabled;
 * See: https://docs.datadoghq.com/real_user_monitoring/browser/
 *
 * /!\ If you want to use RUM, do not create/ask for a separate client token
 *     for browser log, but use the client token generated along the RUM app.
 *
 * See `src/utils/instrument.ts`
 */
const datadogValidator = object({
  DATADOG_CLIENT_TOKEN: string.optional(), // client token
  DATADOG_SERVICE: string.optional(), // e.g. '${PROJECT}.${env}.frontend'
  DATADOG_TEAM: string.optional(), // Datadog 'team' tag
  DATADOG_APP_ID: string.optional(), // for RUM
  DATADOG_SITE: string.optional().default('datadoghq.eu'),
  DATADOG_LEVEL: datadogLevelValidator.optional().default(StatusType.info),
  DATADOG_FORWARD_ERRORS_TO_LOGS: boolean.optional().default(true),
  DATADOG_SILENT_MULTIPLE_INIT: boolean.optional().default(true)
}).map(o => ({
  datadogToken: o.DATADOG_CLIENT_TOKEN,
  datadogService: o.DATADOG_SERVICE,
  datadogTeam: o.DATADOG_TEAM,
  datadogAppId: o.DATADOG_APP_ID,
  datadogSite: o.DATADOG_SITE,
  datadogLevel: o.DATADOG_LEVEL,
  datadogForwardErrorsToLogs: o.DATADOG_FORWARD_ERRORS_TO_LOGS,
  datadogSilentMultipleInit: o.DATADOG_SILENT_MULTIPLE_INIT
}))

export const envValidator = intersection(baseValidator, datadogValidator)

export type AppEnv = typeof envValidator.T
