interface Options {
  version: string
  script: string
  css: string
}

export function indexTemplate(options: Options) {
  const { version, script, css } = options

  const appTitle = 'CHANGE - ME'

  return `
<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="description" content="${appTitle} (${version})" />
    <title>${appTitle}</title>
    ${css}
  </head>

  <body>
    <div id="app"></div>
    ${script}
  </body>
</html>`
}
