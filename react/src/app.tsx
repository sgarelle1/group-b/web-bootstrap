import React, { FC } from 'react'
import { AppRoute, useRoute } from 'src/components/router'
import { PrimaryMenu } from 'src/components/primaryMenu/primaryMenu'
import { Blue } from 'src/screens/blue/blue'
import { Green } from 'src/screens/green/green'
import Logo from '@hector/design-tokens/dist/assets/logo/groupe-ratp.svg'
import '@hector/web-styles/dist/hector-web-styles.min.css'
import 'src/theme/reset.scss'
import 'src/theme/global.scss'
import styles from './app.module.scss'

const Version = 'ApplicationVersion' // replaced at build-time

export const App: FC = () => {
  const route = useRoute()

  return (
    <div className={styles.app}>
      <Logo width={140} />
      <h1>App {Version}</h1>
      <PrimaryMenu />
      <main className={styles.main}>{renderCurrentRoute(route)}</main>
    </div>
  )
}

function renderCurrentRoute(route: AppRoute) {
  switch (route.name) {
    case 'index':
      return 'Index'
    case 'shape':
      return <Blue routeParams={route.params} />
    case 'shapes':
      return <Green />
    case 'notFound':
      return '404'
  }
}
