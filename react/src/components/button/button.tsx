import React, { FC } from 'react'
import { makeClassName } from 'src/utils/className'

interface Props {
  /** Whether this button is the primary CTA button. Defaults to false. */
  primary?: boolean

  /** The size of this button. Defaults to 'large'. */
  size?: 'small' | 'large'

  /** An extra className to be added to this Button. Mostly useful to position it. */
  className?: string
}

export const Button: FC<Props> = props => {
  const { primary = false, size = 'large', children } = props

  const className = makeClassName(
    'he-Button',
    primary ? 'he-Button--primary' : 'he-Button--secondary',
    `he-Button--${size}`,
    props.className
  )

  return <button className={className}>{children}</button>
}
