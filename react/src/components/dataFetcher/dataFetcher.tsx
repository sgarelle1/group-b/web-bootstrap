import { vfetch, VFetchResult } from 'src/utils/fetch'
import React, { FC, useState, useEffect } from 'react'
import type { Validator } from 'idonttrustlikethat'

interface Props<T> {
  route: string
  validator: Validator<T>
  children: FC<T>
}

export function DataFetcher<T>({ children, route, validator }: Props<T>) {
  const data: VFetchResult<T> | undefined = useData(route, validator)
  if (!data) return <p>Loading...</p>
  if (data && !data.ok) return <p>Error... ({JSON.stringify(data.error, null, 2)})</p>

  return children(data.value)
}

function useData<T>(route: string, validator: Validator<T>): VFetchResult<T> | undefined {
  const [data, setData] = useState<VFetchResult<T>>()

  function getData() {
    return vfetch(route, { ok: validator })
  }

  useEffect(() => {
    getData().then(setData)
  }, [route])

  return data
}
