import React, { FC } from 'react'
import { fileHash } from 'src/_generated/public.hash'

type IconName = 'example'

interface Props {
  name: IconName
  size?: number
  color?: string
}

export const Icon: FC<Props> = ({ size = 24, color, name }) => {
  const path = `icons/${name}.svg`
  const h = fileHash(path)

  if (!h) {
    throw new Error(`Fails to resolve path for icon: ${name}`)
  }

  const iconPath = `/icons/${name}.${h}.svg#icon`

  return (
    <svg width={size} height={size} style={color ? { color } : undefined}>
      <use xlinkHref={iconPath} />
    </svg>
  )
}
