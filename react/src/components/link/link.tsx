import React, { FC, useRef } from 'react'
import type { RouteAndParams } from 'typescript-router'
import { router } from 'src/components/router'
import type { AppRouter } from 'src/components/router'

interface Props {
  className?: string
  route: RouteAndParams<AppRouter>
  /* Whether the current history entry should be replaced instead of a new entry being created. */
  replace?: boolean
  /* By default, the navigation occurs on mousedown, which feels faster than waiting for a full click. */
  navigateOnClick?: boolean
}

export const Link: FC<Props> = props => {
  const { route, children, replace, navigateOnClick = false } = props
  const href = router.link(route[0], route[1])

  const preventClickDefault = useRef(false)

  function onMouseDown(evt: React.MouseEvent<HTMLAnchorElement>) {
    if (navigateOnClick) return

    preventClickDefault.current = false

    const navigated = maybeNavigate(evt)
    if (navigated) {
      preventClickDefault.current = true
    }
  }

  function onClick(evt: React.MouseEvent<HTMLAnchorElement>) {
    if (navigateOnClick) {
      if (maybeNavigate(evt)) evt.preventDefault()
      return
    }

    if (preventClickDefault.current) {
      preventClickDefault.current = false
      evt.preventDefault()
    }
  }

  function maybeNavigate(evt: React.MouseEvent<HTMLAnchorElement>) {
    const isModifiedEvent = Boolean(
      evt.metaKey || evt.altKey || evt.ctrlKey || evt.shiftKey
    )

    const isSelfTarget =
      !evt.target || !evt.currentTarget.target || evt.currentTarget.target === '_self'

    if (
      isSelfTarget && // Ignore everything but links with target self
      evt.button === 0 && // ignore everything but left clicks
      !isModifiedEvent // ignore clicks with modifier keys
    ) {
      if (replace) router.replace(route[0], route[1])
      else router.push(route[0], route[1])

      return true
    }

    return false
  }

  return (
    <a
      className={props.className}
      href={href}
      onMouseDown={onMouseDown}
      onClick={onClick}
    >
      {children}
    </a>
  )
}
