import React, { FC, memo } from 'react'
import { Link } from 'src/components/link/link'
import { makeShapeId } from 'shared/model/shape'
import styles from './primaryMenu.module.scss'
import { makeClassName } from 'src/utils/className'

const PrimaryMenuComponent: FC = () => {
  const linkClassName = 'he-Navbar-primaryLink'
  const navbarClassName = makeClassName('he-Navbar', styles.menu)
  const logoLinkClassName = makeClassName('he-Navbar-brand', styles.logo)

  return (
    <div className={navbarClassName}>
      <button className="he-Navbar-toggler" />
      <Link route={['index', {}]} className={logoLinkClassName}>
        <img
          className="he-Navbar-logo"
          src="/assets/logo/logo-ratp-color-horiz.svg"
          alt="ratp"
          title="ratp"
        />
      </Link>
      <div className="he-Navbar-separator" />
      <div className="he-Navbar-collapse">
        <nav className="he-Navbar-primaryNav">
          <Link route={['index', {}]} className={linkClassName}>
            Index
          </Link>
          <Link route={['shapes', {}]} className={linkClassName}>
            Shapes
          </Link>
          <Link route={['shape', { id: makeShapeId('2') }]} className={linkClassName}>
            Shape
          </Link>
        </nav>
      </div>
    </div>
  )
}

PrimaryMenuComponent.displayName = 'PrimaryMenu'

export const PrimaryMenu = memo(PrimaryMenuComponent)
