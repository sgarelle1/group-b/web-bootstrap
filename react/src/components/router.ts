import { useEffect, useState } from 'react'
import { Route, Router } from 'typescript-router'
import { withId } from 'shared/model/shape'

export const router = Router(
  {
    index: Route('/'),
    shapes: Route('/shapes'),
    shape: Route('/shapes/:id', withId)
  },
  { onNotFound: reason => console.error(reason) }
)

export type AppRouter = typeof router
export type AppRoute = AppRouter['route']

export function useRoute() {
  const [state, setState] = useState(router.route)

  useEffect(() => {
    return router.onChange(() => {
      setState(router.route)
    })
  })

  return state
}
