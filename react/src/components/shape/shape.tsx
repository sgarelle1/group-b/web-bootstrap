import React, { FC } from 'react'
import type { ApiShape } from 'shared/model/shape'

interface Props {
  shape: ApiShape
}

export const Shape: FC<Props> = ({ shape }) => {
  if ('size' in shape) {
    return (
      <>
        <h3>This is a square!</h3>
        <p>
          <em>Size:</em> {shape.size}
        </p>
      </>
    )
  } else {
    return (
      <>
        <h3>This is a triangle!</h3>
        <p>
          <em>Base:</em> {shape.base}
        </p>
        <p>
          <em>Height:</em> {shape.height}
        </p>
      </>
    )
  }
}
