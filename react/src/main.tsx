import React from 'react'
import ReactDOM from 'react-dom'
import { App } from './app'

const appElement = document.getElementById('app')

if (!appElement) throw new Error('The html template is broken and missing #app')

ReactDOM.render(<App />, appElement)
