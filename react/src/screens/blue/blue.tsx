import React, { FC } from 'react'
import type { RouteParams } from 'typescript-router'
import { DataFetcher } from 'src/components/dataFetcher/dataFetcher'
import type { AppRouter } from 'src/components/router'
import { Shape } from 'src/components/shape/shape'
import { apiShape, ApiShape } from 'shared/model/shape'
import styles from './blue.module.scss'
import { Button } from 'src/components/button/button'

interface Props {
  routeParams: RouteParams<AppRouter, 'shape'>
}

export const Blue: FC<Props> = ({ routeParams }) => {
  const route = `${appEnv.backendUrl}/api/shapesPg/${routeParams.id}`

  return (
    <div className={styles.blue}>
      <h2>User #{routeParams.id}</h2>
      <DataFetcher route={route} validator={apiShape}>
        {(data: ApiShape) => <Shape shape={data} />}
      </DataFetcher>

      <Button primary className={styles.button}>
        Dummy button
      </Button>
    </div>
  )
}
