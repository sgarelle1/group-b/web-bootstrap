import { array } from 'idonttrustlikethat'
import React, { FC } from 'react'
import { DataFetcher } from 'src/components/dataFetcher/dataFetcher'
import { Icon } from 'src/components/icon/icon'
import { Link } from 'src/components/link/link'
import { apiShape, ApiShape } from 'shared/model/shape'
import styles from './green.module.scss'

interface Props {}

const route = `${appEnv.backendUrl}/api/shapesPg`

export const Green: FC<Props> = () => (
  <div className={styles.green}>
    <h2>Shapes</h2>
    <p>
      This is an icon
      <Icon name="example" color="#992222" />
    </p>
    <DataFetcher route={route} validator={array(apiShape)}>
      {(data: ApiShape[]) => (
        <ul>
          {data.map(shape => (
            <li key={shape.id}>
              <Link route={['shape', { id: shape.id }]}>Shape {shape.id}</Link>
            </li>
          ))}
        </ul>
      )}
    </DataFetcher>
  </div>
)
