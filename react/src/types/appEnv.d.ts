import type { AppEnv } from '../../build/envValidator'

declare global {
  var appEnv: AppEnv
}
