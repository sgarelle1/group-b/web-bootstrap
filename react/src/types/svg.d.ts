// With the svgr plugin, any svg file is imported as a React component.
declare module '*.svg' {
  import type { FC, SVGProps } from 'react'

  const content: FC<SVGProps<{}>>
  export default content
}
