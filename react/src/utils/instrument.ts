import { datadogLogs, Logger, HandlerType, StatusType } from '@datadog/browser-logs'
import { datadogRum } from '@datadog/browser-rum'

type DatadogClient = {
  token: string // client token
  service: string // e.g. '${PROJECT}.${env}.frontend'
  team: string
  applicationId?: string // Only for RUM
}

type CommonSettings = {
  env: string
  site: string
  forwardErrorsToLogs: boolean
  silentMultipleInit: boolean
  level: StatusType
}

type LoggingConfiguration = {
  client?: DatadogClient
} & CommonSettings

type DatadogConfiguration = {
  client: DatadogClient
  version: string
  level: StatusType
} & CommonSettings

export type LoggerProfile = {
  id: string
  name: string
  sessionId: string
}

function datadogLoggerFactory(
  conf: DatadogConfiguration
): (profile: LoggerProfile) => Logger {
  return profile => {
    console.log(
      `Initializing Datadog logger for ${profile.name} (${profile.sessionId}) ...`
    )

    datadogLogs.init({
      clientToken: conf.client.token,
      service: conf.client.service,
      site: conf.site,
      forwardErrorsToLogs: conf.forwardErrorsToLogs,
      silentMultipleInit: conf.silentMultipleInit,
      version: conf.version,
      env: `${conf.env},team:${conf.client.team}`
    })

    // See https://docs.datadoghq.com/logs/log_configuration/attributes_naming_convention/#user-related-attributes
    datadogLogs.setLoggerGlobalContext({
      'usr.id': profile.id,
      'usr.name': profile.name,
      session_id: profile.sessionId
    })

    const logger = datadogLogs.logger

    logger.setLevel(conf.level)

    logger.info(`Datadog logger init'ed for ${profile.name}`)

    if (conf.client.applicationId !== undefined) {
      datadogRum.init({
        applicationId: conf.client.applicationId,
        clientToken: conf.client.token,
        site: conf.site,
        service: conf.client.service,
        env: `${conf.env},team:${conf.client.team}`,
        version: conf.version,
        sampleRate: 100, // TODO: Conf
        trackInteractions: true,
        silentMultipleInit: conf.silentMultipleInit
      })

      datadogRum.setUser({
        id: profile.id,
        name: profile.name
      })

      // API user session != browser session ID
      datadogRum.setRumGlobalContext({
        'api.session_id': profile.sessionId
      })

      logger.info(`Datadog RUM init'ed for ${profile.name}`)
    }

    return logger
  }
}

export const consoleLogger: Logger = new Logger(_msg => ({}), HandlerType.console)

// See `src/store/instrument.ts`
const consoleLoggerFactory: (profile: LoggerProfile) => Logger = _profile => {
  console.log('Applying console logger ...')

  return consoleLogger
}

// ---

// Load configuration from appEnv
const datadogClient =
  appEnv.datadogToken !== undefined &&
  appEnv.datadogService !== undefined &&
  appEnv.datadogTeam !== undefined
    ? {
        token: appEnv.datadogToken,
        service: appEnv.datadogService,
        team: appEnv.datadogTeam,
        applicationId: appEnv.datadogAppId
      }
    : undefined

const conf: LoggingConfiguration = {
  env: appEnv.env,
  client: datadogClient,
  site: appEnv.datadogSite,
  level: appEnv.datadogLevel,
  forwardErrorsToLogs: appEnv.datadogForwardErrorsToLogs,
  silentMultipleInit: appEnv.datadogSilentMultipleInit
}

export const loggerFactory =
  conf.client !== undefined
    ? datadogLoggerFactory({
        ...conf,
        version: 'ApplicationVersion', // replaced at build-time
        client: conf.client
      })
    : consoleLoggerFactory
