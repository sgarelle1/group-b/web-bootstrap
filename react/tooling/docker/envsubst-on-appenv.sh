# Generate app-env according runtime environment variables
WEBROOT="/usr/share/nginx/html"

TMP_ENV=`mktemp`

if [ "x$BACKEND_URL" = "x" ]; then
    echo "Missing environment variable: BACKEND_URL = host & port to reach the backend API"
    exit 1
fi

if [ "x$APP_ENV" = "x" ]; then
    echo "Missing environment variable: APP_ENV = Application environment"
    exit 1
fi

# Datadog settings
if [ ! "x${DATADOG_CLIENT_TOKEN}" = "x" -a ! "x${DATADOG_SERVICE}" = "x" -a ! "x${DATADOG_SITE}" = "x" ]; then
  DATADOG_CLIENT_TOKEN="\"${DATADOG_CLIENT_TOKEN}\""
  DATADOG_SERVICE="\"${DATADOG_SERVICE}\""
  DATADOG_SITE="\"${DATADOG_SITE}\""
else
  DATADOG_CLIENT_TOKEN="undefined"
  DATADOG_SERVICE="undefined"
  DATADOG_SITE="undefined"
fi

if [ ! "x$DATADOG_APP_ID" = "x" ]; then
    DATADOG_APP_ID="\"${DATADOG_APP_ID}\""
else
    DATADOG_APP_ID="undefined"
fi

if [ ! "x$DATADOG_LEVEL" = "x" ]; then
    DATADOG_LEVEL="\"${DATADOG_LEVEL}\""
else
    DATADOG_LEVEL="undefined"
fi

# Output as JSON configuration
cat > "$TMP_ENV" <<EOF
var appEnv={
  IS_PROD:true,
  BACKEND_URL:"$BACKEND_URL",
  APP_ENV:"$APP_ENV",
  DATADOG_CLIENT_TOKEN:$DATADOG_CLIENT_TOKEN,
  DATADOG_SERVICE:$DATADOG_SERVICE,
  DATADOG_APP_ID:$DATADOG_APP_ID,
  DATADOG_SITE:$DATADOG_SITE,
  DATADOG_LEVEL:$DATADOG_LEVEL,
  DATADOG_FORWARD_ERRORS_TO_LOGS:true,
  DATADOG_SILENT_MULTIPLE_INIT:true
}
EOF

# Publish as immutable asset
MD5_SUM=`md5sum "$TMP_ENV" | cut -d ' ' -f 1`
APP_ENV_FILENAME="app-env.${MD5_SUM}.js"
APP_ENV="$WEBROOT/$APP_ENV_FILENAME"

INDEX="$WEBROOT/index.html"
sed -e "s|/app-env.*\.js'|/${APP_ENV_FILENAME}'|" < "$INDEX" > "${INDEX}.tmp"

mv "$TMP_ENV" "$APP_ENV"
chmod a+r "$APP_ENV"
mv "${INDEX}.tmp" "$INDEX"
