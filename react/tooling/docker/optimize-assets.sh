DIR="$1"

for F in `ls -v -1 "$DIR/"*.* "$DIR/"*/*.* | grep -v '.gz'`; do
  echo ""
  echo "Try to compress $F ..."

  cp "$F" "$F.bk"

  R=`gzip -9v "$F" 2>&1 | cut -d ':' -f 2 | sed -e 's/^[ \t]*//;s/\.[0-9]*%.*$//'`

  if [ "$R" -lt 10 ]; then
    echo "... $F is already compressed"

    rm -f "$F.gz"
    mv "$F.bk" "$F"
  else
    echo "... $F successfully compressed"

    mv "$F.bk" "$F"
  fi
done
