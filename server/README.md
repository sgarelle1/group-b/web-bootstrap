# Fastify Bootstrap

## Prerequisites

Install `yarn` globally. All other dependencies are local.

- [NodeJS](https://nodejs.org/en/) > 14 is installed
- [yarn](https://www.npmjs.com/package/yarn) is installed
- [Docker](https://www.docker.com/get-started) is installed

## Running the app in dev mode

_1/ Set environment variables_
Environment variables can be set by creating a `.env` file locally, besides the file `.env.example` (for inspiration).

_2/ Run locally the services (e.g. PostgreSQL ...) using Docker_
In a first console, start Docker components:

    yarn docker-up

_3/ Run application_
In a second console, start the application:

    yarn dev

## Building the app for distribution

First build the distribution from the sources:

```
yarn version --new-version $NEW_VERSION --no-git-tag-version
yarn build
```

Then a corresponding Docker image can be created:

    docker build -t fastify-backend:$NEW_VERSION . -f tooling/docker/Dockerfile

Finally a Docker container can be executed:

```
docker run -P --rm -it fastify-backend:$NEW_VERSION

# With settings as environment variables
docker run -P -e POSTGRESQL_URL=... --rm -it fastify-backend:$NEW_VERSION
```

> The server is accessible through the public port assigned by the Docker engine (see `docker ps`).

## Project structure

```
├── nodemon.json      Config file for file watcher Nodemon
├── package.json      Project metadata (dependencies, scripts, version, ...)
│   └── docker        Docker related tools and configuration
├── src
│   ├── app.ts        Builds the Fastify app adding plugins, routers, ...
│   ├── components    Business components of the application
│   ├── core          Core logic of the application (logger, environment variables, ...)
│   ├── server.ts     Entry point of the application starting the server
│   └── typings       Specific typings for the application
├── tooling           Tooling ressources (Docker, Terraform, ...)
├── tsconfig.json     Specifies the root files and the compiler options required to compile the project.
└── yarn.lock         Stores every version of every dependency installed
```

### Components

A component will most of the time contain specific files following a Domain Driven Design like approach.

#### Controller (presentation layer)

Merely responsible to presenting a business function.
Hence it mostly delegates to a Service, still taking care of presentation-related logic (parsing, validating).

#### Service (application service layer)

High level speaking, it represents business use cases.
This layer mostly make use of domain layer to compose of a meaningful use case.
It is responsible for data transformations, and delegations to domain layer artifacts.

#### Repository (domain layer)

It's a gateway between your domain layer and a data mapping layer, which is the layer that accesses the database and does the operations.
Basically the repository is an abstraction to you database access.

## Environment variables
