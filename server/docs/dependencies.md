# Dependencies

## mongo

The official Mongo driver is used to connect to the database (`mongodb` on npm)

## node-sql-ts

This [library](https://github.com/charsleysa/node-sql-ts) is a SQL string builder for Node.
It is built with Typescript and provide a good way to check your queries at compile time.
Your schemas evolutions will for example trigger compilation error which will guide you.
This should not prevent you from doing integration tests: the compilation check is
based on a meta-model written by yourself and testing remains the only solution
you have to validate your queries are typed correctly.
