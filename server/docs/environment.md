# Environment variables

Environment variables can be set by creating a `.env` file locally, besides the file `.env.example` (for inspiration).

They are validated by file [environment.ts](/server/src/core/environment.ts).

Here is a list of all the possible environment variables. The one marked with a `*` are mandatory.

| Nom                           | Description                                                                                                                                                            | Default        |
| ----------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------- |
| APP_NAME                      | Name of the application                                                                                                                                                | Web bootstrap  |
| API_HOST                      | Host on which the API will listen                                                                                                                                      | 0.0.0.0        |
| API_PORT                      | Port on which the API will listen                                                                                                                                      | 5001           |
| API_PREFIX                    | Prefix used by the API                                                                                                                                                 |                |
| API_PROTOCOL                  | Protocol used by the API                                                                                                                                               | http           |
| AUTOMATIC_MIGRATION           | If migrations should play at startup                                                                                                                                   |                |
| EXTERNAL_URL                  | External URL of this server                                                                                                                                            |                |
| NODE_ENV\*                    | Node environment ('production', 'development', 'test')                                                                                                                 |                |
| SHOULD_MOCK_DATA              | Whether the database should be initialized with mocked data                                                                                                            | false          |
| POSTGRESQL_URL\*              | Connection URL for database                                                                                                                                            |                |
| POSTGRESQL_CONNECTION_TIMEOUT | Connection timeout for database                                                                                                                                        | 5000           |
| MONGODB_URI\*                 | [Connection URI](https://docs.mongodb.com/manual/reference/connection-string/) for MongoDB                                                                             |                |
| KAFKA_BOOTSTRAP_SERVERS       | Comma-separated list of host and port pairs that are the addresses of the initial (bootstrap) Kafka brokers                                                            | localhost:9092 |
| KAFKA_SECURITY_PROTOCOL       | [Kafka security protocol](https://kafka.apache.org/24/javadoc/org/apache/kafka/common/security/auth/SecurityProtocol.html): `PLAINTEXT`, `SASL_{PLAINTEXT,SSL}`, `SSL` |                |
| KAFKA_JAAS_CONFIG             | JAAS configuration for Kafka connections                                                                                                                               |                |
| KAFKA_SASL_MECHANISM          | SASL mechanism for Kafka connections                                                                                                                                   |                |
