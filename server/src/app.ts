import {
  FastifyInstance,
  fastify,
  onRequestHookHandler,
  onResponseHookHandler
} from 'fastify'
import fastifyCors, { FastifyCorsOptions } from 'fastify-cors'

import { shapeModule as shapeModulePg } from '@components/shapesPg/shapes.module'
import { shapeModule as shapeModuleMongo } from '@components/shapesMongo/shapes.module'
import { Logger } from '@core/logger'
import type { PG } from '@core/database/pg/wrapper'
import type { Mongo } from '@core/database/mongo/wrapper'

const logger = Logger.child({ name: 'app' })

export const build = (pg: PG, mongo: Mongo) => {
  const app = fastify()
  configureHooks(app, pg, mongo)
  configurePlugins(app)
  const modules = {
    shapePg: shapeModulePg(pg),
    shapeMongo: shapeModuleMongo(mongo)
  }
  registerRoutes(app, modules.shapePg.router, modules.shapeMongo.router)
  return { app, modules }
}

const configureHooks = (app: FastifyInstance, pg: PG, mongo: Mongo) => {
  const logResponseTime: onResponseHookHandler = (req, res, done) => {
    logger.debug(
      `${req.method} - ${req.url} - ${res.statusCode} - ExecTime: ${Math.round(
        res.getResponseTime()
      )}ms`
    )
    done()
  }

  const logHeaders: onRequestHookHandler = (req, _, done) => {
    logger.debug(`${req.method} - ${req.url} - ${JSON.stringify(req.headers, null, 2)}`)
    done()
  }

  // hooks
  app.addHook('onRequest', logHeaders)
  app.addHook('onResponse', logResponseTime)
  app.addHook('onClose', pg.onShutdown)
  app.addHook('onClose', mongo.onShutdown)
  app.addHook('onError', (req, _, error, done) => {
    logger.debug({
      description: `${req.method} - ${req.url} - Outputed an error`,
      error
    })
    done()
  })
}

const configurePlugins = (app: FastifyInstance) => {
  let corsConfig: FastifyCorsOptions = {}
  if (global.env.NODE_ENV !== 'development') {
    if (global.env.EXTERNAL_URL) {
      corsConfig.origin = global.env.EXTERNAL_URL.replace(/([^/]):[0-9]+/, '$1')
    }
  }
  app.register(fastifyCors, corsConfig)
}

const registerRoutes = (
  app: FastifyInstance,
  ...routers: Array<(_: FastifyInstance) => Promise<void>>
) => {
  const prefix = global.env.API_PREFIX
  routers.forEach(router => app.register(router, { prefix }))
}
