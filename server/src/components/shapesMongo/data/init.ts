import { Logger } from '@core/logger'
import type { Shape } from '@shared/model/shape'
import type { ShapeService } from '../shapes.service'

const shapes: Shape[] = [{ size: 17 }, { base: 3, height: 4 }]
export const initializeWithMockedData = async (service: ShapeService) => {
  const count = await service.count()
  if (count === 0) {
    await Promise.all(shapes.map(service.create))
  } else {
    Logger.debug('Shape collection already initialized')
  }
}
