import { ObjectID } from 'bson'
import type { ShapeRepository } from '../shapes.repository'

export const mockRepository: ShapeRepository = {
  create: () => Promise.resolve(new ObjectID()),
  update: () => Promise.resolve(1),
  count: () => Promise.resolve(17),
  list: () => Promise.resolve([{ _id: new ObjectID(), size: 17 }]),
  get: _id => Promise.resolve({ _id, size: 17 }),
  delete: () => Promise.resolve(1)
}
