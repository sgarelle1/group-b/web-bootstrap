import { intersection, object, union } from 'idonttrustlikethat'
import { objectId } from '@core/database/mongo//objectId'
import { squareSchema, triangleSchema } from '@shared/model/shape'

export const withId = object({ _id: objectId })

export const shapeDocument = union(
  intersection(squareSchema, withId),
  intersection(triangleSchema, withId)
)
export type ShapeDocument = typeof shapeDocument.T
