import assert from 'assert'
import { ObjectId, OptionalId } from 'mongodb'

import { getDatabase } from '@core/database/mongo/getDatabase'
import type { Mongo } from '@core/database/mongo/wrapper'
import { validateEnvVariables } from '@core/environment'
import { MongoShapeRepository, collectionName } from './mongo.shapes.repository'
import type { ShapeDocument } from './mongo.shapes.models'

global.env = validateEnvVariables({ path: '.env.test', debug: true })

describe('MongoShapeRepository', () => {
  let _mongo: Mongo
  let repository: MongoShapeRepository

  before(async () => {
    _mongo = await getDatabase()
    const collection = await _mongo.getCollection(collectionName)
    await collection.drop()
    repository = MongoShapeRepository(_mongo)
  })

  after(async () => {
    await _mongo.onShutdown()
  })

  describe('get', () => {
    it('should return undefined if object is not in database', async () => {
      // Given
      const id = new ObjectId()
      // When
      const result = await repository.get(id)
      // Then
      assert.deepStrictEqual(result, undefined)
    })

    it('should return the correct object if it is in database', async () => {
      // Given
      const square: OptionalId<ShapeDocument> = { size: 17 }
      // When
      const id = await repository.create(square)
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const document = await repository.get(id)
      // Then
      assert.deepStrictEqual(document, { _id: id, ...square })
    })

    it('should return the correct object if there is an other object in database', async () => {
      // Given
      const triangle: OptionalId<ShapeDocument> = { base: 5, height: 3 }
      // When
      const id = await repository.create(triangle)
      const result = await repository.get(id)
      // Then
      assert.deepStrictEqual(result, { _id: id, ...triangle })
    })
  })
})
