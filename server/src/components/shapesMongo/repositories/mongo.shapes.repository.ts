import type { ObjectID, OptionalId } from 'mongodb'
import type { Mongo } from '@core/database/mongo/wrapper'
import { validateDocument, validateDocumentOpt } from '@core/database/mongo/utils'
import { Logger } from '@core/logger'
import type { Shape } from '@shared/model/shape'

import type { ShapeRepository } from '../shapes.repository'
import { shapeDocument, ShapeDocument } from './mongo.shapes.models'

const logger = Logger.child({ name: 'MongoShapeRepository' })

export const collectionName = 'shape'

export type MongoShapeRepository = ReturnType<typeof MongoShapeRepository>

export function MongoShapeRepository(mongo: Mongo): ShapeRepository {
  return {
    create: async (shape: OptionalId<ShapeDocument>): Promise<ObjectID> => {
      const collection = await mongo.getCollection<Shape>(collectionName)
      const result = await collection.insertOne(shape)
      return result.insertedId
    },

    count: async (): Promise<number> => {
      const collection = await mongo.getCollection<Shape>(collectionName)
      return collection.countDocuments()
    },

    list: async (): Promise<ShapeDocument[]> => {
      logger.debug(`List shapes`)
      const collection = await mongo.getCollection<Shape>(collectionName)
      const results = await collection.find({}, { limit: 10 }).toArray()
      return results.map(document =>
        validateDocument(shapeDocument, 'list_shape', document, logger)
      )
    },

    get: async (_id: ObjectID): Promise<ShapeDocument | undefined> => {
      logger.debug(`Lookup for shape '${_id}'`)
      const collection = await mongo.getCollection<Shape>(collectionName)
      const document = await collection.findOne({ _id })
      return validateDocumentOpt(shapeDocument, 'get_shape', document, logger)
    },

    update: async (_id: ObjectID, shape: OptionalId<ShapeDocument>): Promise<number> => {
      const collection = await mongo.getCollection<Shape>(collectionName)
      const result = await collection.findOneAndUpdate({ _id }, shape)
      return result.ok ?? 0
    },

    delete: async (_id: ObjectID): Promise<number> => {
      const collection = await mongo.getCollection<Shape>(collectionName)
      const result = await collection.deleteOne({ _id })
      return result.deletedCount ?? 0
    }
  }
}
