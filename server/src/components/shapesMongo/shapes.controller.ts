import type { FastifyRequest, FastifyReply } from 'fastify'

import { Logger } from '@core/logger'
import { getBody, getPathParams } from '@core/helpers/controllers'
import { notFound } from '@core/api/errors'
import { created, ok } from '@core/api/success'
import { shape } from '@shared/model/shape'

import type { ShapeService } from './shapes.service'
import { objectId } from '@core/database/mongo/objectId'
import { object } from 'idonttrustlikethat'

const logger = Logger.child({ name: 'ShapeController ' })

const withId = object({
  id: objectId
})

export type ShapeController = ReturnType<typeof ShapeController>

export function ShapeController(service: ShapeService) {
  return {
    createShape: async (req: FastifyRequest, rep: FastifyReply): Promise<void> => {
      const body = getBody(shape, req, rep)
      logger.debug({ description: 'Create', body })
      const createdShape = await service.create(body)

      return created(rep, { id: createdShape })
    },

    listShapes: async (_: FastifyRequest, rep: FastifyReply): Promise<void> => {
      logger.debug({ description: 'List' })
      const shapes = await service.list()

      return ok(rep, shapes)
    },

    getShape: async (req: FastifyRequest, rep: FastifyReply): Promise<void> => {
      const { id } = getPathParams(withId, req, rep)
      logger.debug({ description: 'Lookup', id })
      const shapeResult = await service.get(id)

      if (!shapeResult) return notFound(rep, { description: `Shape '${id}' not found.` })

      return ok(rep, shape)
    },

    updateShape: async (req: FastifyRequest, rep: FastifyReply): Promise<void> => {
      const { id } = getPathParams(withId, req, rep)
      const body = getBody(shape, req, rep)
      logger.debug({ description: 'Update', id, body })
      const updatedShape = await service.update(id, body)

      if (!updatedShape) return notFound(rep, { description: `Shape '${id}' not found.` })

      return ok(rep, updatedShape)
    },

    deleteShape: async (req: FastifyRequest, rep: FastifyReply): Promise<void> => {
      const { id } = getPathParams(withId, req, rep)
      logger.debug({ description: 'Delete', id })
      const count = await service.delete(id)

      if (count === 0) return notFound(rep, { description: `Shape '${id}' not found.` })

      return ok(rep, {})
    }
  }
}
