import type { Mongo } from '@core/database/mongo/wrapper'
import { ShapeController } from './shapes.controller'
import { router as shapeRouter } from './shapes.routes'
import { ShapeService } from './shapes.service'
import { mockRepository } from './repositories/mock.shapes.repository'
import { MongoShapeRepository } from './repositories/mongo.shapes.repository'

export const shapeModule = (mongo?: Mongo) => {
  const repository = mongo ? MongoShapeRepository(mongo) : mockRepository
  const service = ShapeService(repository)
  const controller = ShapeController(service)
  const router = shapeRouter(controller)
  return { repository, service, controller, router }
}
