import type { ObjectID, OptionalId } from 'mongodb'
import type { ShapeDocument } from './repositories/mongo.shapes.models'

export interface ShapeRepository {
  create(shape: OptionalId<ShapeDocument>): Promise<ObjectID>
  update(_id: ObjectID, shapeEntry: OptionalId<ShapeDocument>): Promise<number>
  delete(_id: ObjectID): Promise<number>

  count(): Promise<number>
  get(_id: ObjectID): Promise<ShapeDocument | undefined>
  list(): Promise<ShapeDocument[]>
}
