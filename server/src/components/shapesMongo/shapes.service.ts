import type { ObjectID } from 'mongodb'
import { Logger } from '@core/logger'
import { ApiShape, makeShapeId, Shape, ShapeId } from '@shared/model/shape'
import type { ShapeDocument } from './repositories/mongo.shapes.models'
import type { ShapeRepository } from './shapes.repository'

const logger = Logger.child({ name: 'ShapeService' })

const shapeDocumentToApi = ({ _id, ...rest }: ShapeDocument): ApiShape => ({
  id: makeShapeId(_id.toHexString()),
  ...rest
})

export type ShapeService = ReturnType<typeof ShapeService>

export function ShapeService(repository: ShapeRepository) {
  return {
    create: async (shape: Shape): Promise<ShapeId> => {
      const objectId = await repository.create(shape)
      return makeShapeId(objectId.toHexString())
    },

    list: async (): Promise<ApiShape[] | undefined> => {
      logger.debug('List')
      const results = await repository.list()
      return results.map(shapeDocumentToApi)
    },

    get: async (id: ObjectID): Promise<Shape | undefined> => {
      logger.debug({ id }, 'Lookup')
      const document = await repository.get(id)

      if (!document) return undefined

      return shapeDocumentToApi(document)
    },

    update: async (id: ObjectID, shape: Shape): Promise<ApiShape | undefined> => {
      const updatedCount = await repository.update(id, shape)
      if (updatedCount === 0) return undefined

      return { ...shape, id: makeShapeId(id.toHexString()) }
    },

    delete: async (id: ObjectID): Promise<number> => {
      return repository.delete(id)
    },

    count: async (): Promise<number> => {
      return repository.count()
    }
  }
}
