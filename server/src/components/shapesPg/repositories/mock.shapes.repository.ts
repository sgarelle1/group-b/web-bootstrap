import { makeShapeId } from '@shared/model/shape'
import type { ShapeRepository } from '../shapes.repository'

export const mockRepository: ShapeRepository = {
  create: entry => Promise.resolve(entry),
  update: () => Promise.resolve(1),
  list: () => Promise.resolve([{ id: makeShapeId('4'), size: 17 }]),
  lookup: id => Promise.resolve({ id, size: 17 }),
  delete: () => Promise.resolve(1)
}
