import assert from 'assert'

import { getDatabase } from '@core/database/pg/getDatabase'
import { ShapeEntry, table } from '@core/database/pg/tables/shapes'
import type { PG } from '@core/database/pg/wrapper'
import { validateEnvVariables } from '@core/environment'
import { makeShapeId } from '@shared/model/shape'
import { PgShapeRepository } from './pg.shapes.repository'

global.env = validateEnvVariables({ path: '.env.test', debug: true })

describe('PgShapeRepository', () => {
  let _pg: PG
  let repository: PgShapeRepository

  before(async () => {
    _pg = await getDatabase()
    const dropQuery = table.drop().ifExists().toQuery()
    await _pg.query(dropQuery)
    const createQuery = table.create().ifNotExists().toQuery()
    await _pg.query(createQuery)
    repository = PgShapeRepository(_pg)
  })

  after(async () => {
    await _pg.onShutdown()
  })

  describe('lookup', () => {
    it('should return undefined if object is not in database', async () => {
      // Given
      const id = makeShapeId('unknown_shape')
      // When
      const result = await repository.lookup(id)
      // Then
      assert.deepStrictEqual(result, undefined)
    })

    it('should return the correct object if it is in database', async () => {
      // Given
      const id = makeShapeId('square')
      const square: ShapeEntry = { id, size: 17 }
      // When
      await repository.create(square)
      const result = await repository.lookup(id)
      // Then
      assert.deepStrictEqual(result, square)
    })

    it('should return the correct object if there is an other object in database', async () => {
      // Given
      const id = makeShapeId('triangle') // Square is already in DB from last test
      const triangle: ShapeEntry = { id, base: 5, height: 3 }
      // When
      await repository.create(triangle)
      const result = await repository.lookup(id)
      // Then
      assert.deepStrictEqual(result, triangle)
    })
  })
})
