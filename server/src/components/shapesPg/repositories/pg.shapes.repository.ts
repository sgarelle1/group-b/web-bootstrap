import type { PG } from '@core/database/pg/wrapper'
import type { Repository } from '@core/database/repository'
import type { ShapeId } from '@shared/model/shape'
import { validateRow, validateRowOpt } from '@core/database/pg/utils'
import { table, shapeEntry, ShapeEntry } from '@core/database/pg/tables/shapes'
import { Logger } from '@core/logger'

const logger = Logger.child({ name: 'PgShapeRepository' })

export type PgShapeRepository = Repository<ShapeEntry, ShapeId>

export function PgShapeRepository(pg: PG): PgShapeRepository {
  return {
    create: async (shapeEntry: ShapeEntry): Promise<ShapeEntry> => {
      const query = table.insert(shapeEntry).toQuery()
      logger.debug({ query: query.text })
      await pg.query(query)
      return shapeEntry
    },

    list: async (): Promise<ShapeEntry[]> => {
      logger.debug({ message: `List shapes` })
      const query = table.select(table.star()).from(table).limit(10).toQuery()
      const result = await pg.query(query)
      return result.rows.map(row => validateRow(shapeEntry, 'list_shape', row, logger))
    },

    lookup: async (id: ShapeId): Promise<ShapeEntry | undefined> => {
      logger.debug({ message: `Lookup for shape '${id}'` })
      const query = table
        .select(table.star())
        .from(table)
        .where(table.id.equals(id))
        .limit(1)
        .toQuery()
      const result = await pg.query(query)
      return validateRowOpt(shapeEntry, 'lookup_shape', result.rows[0], logger)
    },

    update: async (entry: ShapeEntry): Promise<number> => {
      const query = table.update(entry).where(table.id.equals(entry.id)).toQuery()
      const result = await pg.query(query)
      return result.rowCount
    },

    delete: async (id: ShapeId): Promise<number> => {
      const query = table.delete().from(table).where(table.id.equals(id)).toQuery()
      const result = await pg.query(query)
      return result.rowCount
    }
  }
}
