import type { PG } from '@core/database/pg/wrapper'
import { ShapeController } from './shapes.controller'
import { router as shapeRouter } from './shapes.routes'
import { ShapeService } from './shapes.service'
import { mockRepository } from './repositories/mock.shapes.repository'
import { PgShapeRepository } from './repositories/pg.shapes.repository'

export const shapeModule = (pg?: PG) => {
  const repository = pg ? PgShapeRepository(pg) : mockRepository
  const service = ShapeService(repository)
  const controller = ShapeController(service)
  const router = shapeRouter(controller)
  return { repository, service, controller, router }
}
