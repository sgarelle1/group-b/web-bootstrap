import type { ShapeEntry } from '@core/database/pg/tables/shapes'
import type { Repository } from '@core/database/repository'
import type { ShapeId } from '@shared/model/shape'

export interface ShapeRepository extends Repository<ShapeEntry, ShapeId> {}
