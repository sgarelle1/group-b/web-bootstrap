import type { FastifyInstance } from 'fastify'
import type { ShapeController } from './shapes.controller'

const endpoint = '/shapesPg'

export const router = (controller: ShapeController) => async (app: FastifyInstance) => {
  app.get(`${endpoint}`, controller.listShapes)
  app.post(`${endpoint}`, controller.createShape)
  app.put(`${endpoint}/:id`, controller.updateShape)
  app.get(`${endpoint}/:id`, controller.lookupShape)
  app.delete(`${endpoint}/:id`, controller.deleteShape)
}
