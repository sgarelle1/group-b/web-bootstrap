import { v4 as uuid } from 'uuid'
import { Logger } from '@core/logger'
import type { ShapeEntry } from '@core/database/pg/tables/shapes'
import { Shape, ShapeId, makeShapeId } from '@shared/model/shape'
import type { ShapeRepository } from './shapes.repository'

const logger = Logger.child({ name: 'ShapeService ' })

export type ShapeService = ReturnType<typeof ShapeService>

export function ShapeService(repository: ShapeRepository) {
  return {
    create: async (shape: Shape): Promise<ShapeEntry> => {
      const id = makeShapeId(uuid())
      return repository.create({ ...shape, id })
    },

    list: (): Promise<Shape[] | undefined> => {
      logger.debug({ description: 'List' })
      return repository.list()
    },

    lookup: async (id: ShapeId): Promise<Shape | undefined> => {
      logger.debug({ description: 'Lookup', id })
      return repository.lookup(id)
    },

    update: async (id: ShapeId, shape: Shape): Promise<ShapeEntry | undefined> => {
      const entry = { ...shape, id }
      const updatedCount = await repository.update(entry)
      if (updatedCount === 0) return undefined
      else return entry
    },

    delete: async (id: ShapeId): Promise<number> => {
      return repository.delete(id)
    }
  }
}
