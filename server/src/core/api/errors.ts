import type { FastifyReply } from 'fastify'
import type { ValidationError } from 'idonttrustlikethat'
import { jsonResponse } from './reponse'

interface ApiError {
  description: string
  detail?: string
}

type BadRequest = ApiError & {
  errors?: ValidationError[]
}

export const badRequest = (rep: FastifyReply, body: BadRequest) =>
  jsonResponse(rep).code(400).send(body)

export const notFound = (rep: FastifyReply, body: ApiError) =>
  jsonResponse(rep).code(404).send(body)
