import type { FastifyReply } from 'fastify'

export const jsonResponse = (rep: FastifyReply) =>
  rep.header('Content-Type', 'application/json; charset=utf-8')
