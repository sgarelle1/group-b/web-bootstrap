import type { FastifyReply } from 'fastify'
import { jsonResponse } from './reponse'

export const ok = <T>(rep: FastifyReply, body: T) =>
  jsonResponse(rep).code(200).send(body)

export const created = <T>(rep: FastifyReply, body: T) =>
  jsonResponse(rep).code(201).send(body)
