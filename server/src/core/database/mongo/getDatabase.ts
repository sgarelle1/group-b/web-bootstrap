import { Mongo } from './wrapper'

export async function getDatabase(): Promise<Mongo> {
  return Mongo({ uri: global.env.MONGODB_URI })
}
