import { object, string } from 'idonttrustlikethat'
import * as path from 'path'
import Umzug, { Storage } from 'umzug'
import { Logger } from '@core/logger'
import type { Mongo } from './wrapper'
import { validateDocument } from './utils'

const logger = Logger.child({ core: 'database', name: 'migration' })
const collectionName = 'migrations'

const migration = object({
  migration: string
})
type Migration = typeof migration.T

const umzug = (mongo: Mongo) =>
  new Umzug({
    migrations: {
      path: path.join(__dirname, './migrations'),
      params: [mongo],
      pattern: /^\d+\.\d+\.\d+-[\w-]+\.[jt]s$/
    },
    storage: CustomStorage(mongo)
  })

export const migrate = async (mongo: Mongo) => {
  logger.info('Playing migrations...')
  const migrator = umzug(mongo)
  try {
    const migrations = await migrator.pending()
    logger.info({
      description: 'Pending migrations',
      migrations: migrations.map(_ => _.migration.name)
    })
    const pendingMigration = await migrator.up()
    if (pendingMigration.length === 0) {
      logger.info('No migration to play at startup')
    } else {
      logger.info(`Ran ${pendingMigration.length} migrations at startup with success`)
    }
  } catch (err) {
    logger.error({ description: 'An error occured while playing migrations', err })
    process.exit(1)
  }
}

export function CustomStorage(mongo: Mongo): Storage {
  return {
    logMigration: async (migrationName: string): Promise<void> => {
      logger.debug({ description: `Logging new migration '${migrationName}'` })
      const collection = await mongo.getCollection<Migration>(collectionName)
      const result = await collection.insertOne({ migration: migrationName })
      logger.debug({ result }, 'Insertion successful')
    },

    unlogMigration: async (migrationName: string): Promise<void> => {
      Logger.debug({ description: `Unlogging migration '${migrationName}'` })
      const collection = await mongo.getCollection<Migration>(collectionName)
      const result = await collection.deleteOne({ migration: migrationName })
      logger.debug({ result }, 'Insertion successful')
    },

    executed: async (): Promise<string[]> => {
      Logger.debug({ description: `Listing executed migrations...` })
      const collection = await mongo.getCollection<Migration>(collectionName)
      const results = await collection.find({}).toArray()
      return results.map(
        row => validateDocument(migration, 'executed_migrations', row, logger).migration
      )
    }
  }
}
export type CustomStorage = ReturnType<typeof CustomStorage>
