import type { Mongo } from '@core/database/mongo/wrapper'

const shapeSizeIndexName = 'shapes_size'
const shapeBaseheightIndexName = 'shapes_base_height'
export const up = async (mongo: Mongo) => {
  const collection = await mongo.getCollection('shapes')
  await Promise.all([
    collection.createIndex(
      { size: 1 },
      {
        name: shapeSizeIndexName,
        partialFilterExpression: { size: { $exists: true } }
      }
    ),
    collection.createIndex(
      {
        base: 1,
        height: 1
      },
      {
        name: shapeBaseheightIndexName,
        partialFilterExpression: { base: { $exists: true } }
      }
    )
  ])
}

export const down = async (mongo: Mongo) => {
  const collection = await mongo.getCollection('shapes')
  await Promise.all([
    collection.dropIndex(shapeSizeIndexName),
    collection.dropIndex(shapeBaseheightIndexName)
  ])
}
