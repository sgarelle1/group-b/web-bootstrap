import { Err, Ok, unknown } from 'idonttrustlikethat'
import { ObjectID } from 'mongodb'

export const objectId = unknown.and(_ => {
  try {
    return Ok(new ObjectID(_ as ObjectID))
  } catch (err) {
    return Err('Given value is not a valid ObjectId')
  }
})
