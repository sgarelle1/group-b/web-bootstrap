import assert from 'assert'
import { number, object, string } from 'idonttrustlikethat'
import { Logger } from '@core/logger'
import { validateDocument, validateDocumentOpt } from './utils'

const validator = object({ foo: string, bar: number })

describe('Mongo Database utils', () => {
  describe('validateDocument', () => {
    it('should return a proper object if validation is successful', () => {
      // Given
      const row = { foo: 'hello', bar: 17 }
      // When
      const result = validateDocument(validator, 'test 1', row, Logger)
      // Then
      assert.deepStrictEqual(result, row)
    })

    it('should return a simpler object if row has undefined fields', () => {
      // Given
      const row = { foo: 'hello', bar: 17, baz: true }
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const { baz, ...expected } = row
      // When
      const result = validateDocument(validator, 'test 1', row, Logger)
      // Then
      assert.deepStrictEqual(result, expected)
    })

    it('should throw if row does not have specified fields', () => {
      // Given
      const row = { foo: 'hello' }
      const name = 'test 1'

      assert.throws(
        () => validateDocument(validator, name, row, Logger),
        /^Error: Could not parse/
      )
    })
  })

  describe('validateDocumentOpt', () => {
    it('should return a proper object if validation is successful', () => {
      // Given
      const row = { foo: 'hello', bar: 17 }
      // When
      const result = validateDocumentOpt(validator, 'test 1', row, Logger)
      // Then
      assert.deepStrictEqual(result, row)
    })

    it('should return a simpler object if row has undefined fields', () => {
      // Given
      const row = { foo: 'hello', bar: 17, baz: true }
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const { baz, ...expected } = row
      // When
      const result = validateDocumentOpt(validator, 'test 1', row, Logger)
      // Then
      assert.deepStrictEqual(result, expected)
    })

    it('should return undefined if row does not have specified fields', () => {
      // Given
      const row = { foo: 'hello' }
      // When
      const result = validateDocumentOpt(validator, 'test 1', row, Logger)
      // Then
      assert.deepStrictEqual(result, undefined)
    })
  })
})
