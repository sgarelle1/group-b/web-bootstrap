import type { Validator } from 'idonttrustlikethat'
import type { Logger } from 'pino'

export function validateDocument<T>(
  validator: Validator<T>,
  name: string,
  document: unknown,
  logger: Logger
): T {
  const validated = validator.validate(document)

  if (!validated.ok) {
    logger.error('Document parsing error %o, %s', validated.errors, document)
    throw new Error(`Could not parse ${name} document`)
  }

  return validated.value
}

export function validateDocumentOpt<T>(
  validator: Validator<T>,
  name: string,
  document: unknown,
  logger: Logger
): T | undefined {
  try {
    return validateDocument(validator, name, document, logger)
  } catch (_) {
    return undefined
  }
}
