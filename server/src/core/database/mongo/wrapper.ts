import { Logger } from '@core/logger'
import { Db, MongoClient, Collection } from 'mongodb'

const UrlParser = require('mongodb/lib/url_parser')

const logger = Logger.child({ name: 'Mongo' })

export type Mongo = ReturnType<typeof Mongo>

export function Mongo(params: {
  uri: string
  connectTimeoutMS?: number
  socketTimeoutMS?: number
}) {
  const { uri, connectTimeoutMS = 500, socketTimeoutMS = 500 } = params
  const parsedUri = UrlParser.parseConnectionString(uri)
  const dbName = parsedUri.dbName.toString()
  let cachedDb: Db | null = null
  let client: MongoClient | null = null

  async function getDatabase(): Promise<Db> {
    if (cachedDb !== null) {
      logger.debug('Using cached db instance')
      return cachedDb
    }
    try {
      logger.debug({ uri })
      client = new MongoClient(uri, {
        useUnifiedTopology: true,
        useNewUrlParser: true,
        connectTimeoutMS,
        socketTimeoutMS
      })
      // Application should connect only once and reuse client instance
      // https://groups.google.com/g/node-mongodb-native/c/mSGnnuG8C1o/m/Hiaqvdu1bWoJ
      await client.connect()
      cachedDb = client.db(dbName)
      logger.info('Connected to database!')
      return cachedDb
    } catch (err) {
      logger.warn(err, 'Error getting a connected database')
      throw err
    }
  }

  async function getCollection<T>(collectionName: string): Promise<Collection<T>> {
    const db = await getDatabase()
    return db.collection<T>(collectionName)
  }

  function onShutdown() {
    if (!client) return

    logger.debug('Using cached db instance')
    return client.close()
  }

  return {
    getCollection,
    onShutdown
  }
}
