import { PG } from '@core/database/pg/wrapper'

export async function getDatabase(): Promise<PG> {
  console.info('global.env.POSTGRESQL_URL', global.env.POSTGRESQL_URL)
  return PG({
    application: { name: global.env.APP_NAME },
    database: {
      uri: global.env.POSTGRESQL_URL,
      connectionTimeout: global.env.POSTGRESQL_CONNECTION_TIMEOUT
    }
  })
}
