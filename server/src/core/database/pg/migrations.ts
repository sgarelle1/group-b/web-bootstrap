import * as path from 'path'
import Umzug, { Storage } from 'umzug'
import { Logger } from '@core/logger'
import type { PG } from './wrapper'
import { validateRow } from './utils'
import { table, migrationEntryValidator } from './tables/_migrations'

const logger = Logger.child({ core: 'database', name: 'migration' })

const umzug = (pg: PG) =>
  new Umzug({
    migrations: {
      path: path.join(__dirname, './migrations'),
      params: [pg],
      pattern: /^\d+\.\d+\.\d+-[\w-]+\.[jt]s$/
    },
    storage: CustomStorage(pg)
  })

export const migrate = async (pg: PG) => {
  logger.info('Playing migrations...')
  const migrator = umzug(pg)
  try {
    const migrations = await migrator.pending()
    logger.info({ msg: 'Pending migrations', migrations: migrations.map(_ => _.file) })
    const pendingMigration = await migrator.up()
    if (pendingMigration.length === 0) {
      logger.info('No migration to play at startup')
    } else {
      logger.info(`Ran ${pendingMigration.length} migrations at startup with success`)
    }
  } catch (err) {
    logger.error({ msg: 'An error occured while playing migrations', err })
    process.exit(1)
  }
}

export function CustomStorage(pg: PG): Storage {
  return {
    logMigration: async (migrationName: string): Promise<void> => {
      logger.debug({ msg: `Logging new migration '${migrationName}'` })
      const query = table.insert({ migration: migrationName }).toQuery()
      await pg.query(query)
    },

    unlogMigration: async (migrationName: string): Promise<void> => {
      logger.debug({ msg: `Unlogging migration '${migrationName}'` })
      const query = table.delete({ migration: migrationName }).toQuery()
      await pg.query(query)
    },

    executed: async (): Promise<string[]> => {
      logger.debug({ msg: `Listing executed migrations...` })
      try {
        const query = table.select(table.migration).from(table).toQuery()
        const result = await pg.query(query)
        logger.warn({ rows: result.rows })
        return result.rows.map(row => {
          return validateRow(migrationEntryValidator, 'executed_migrations', row, logger)
            .migration
        })
      } catch (err) {
        if (err.message === 'relation "public.migration" does not exist') {
          logger.warn({ msg: 'Catched error', err })
          return []
        } else {
          throw err
        }
      }
    }
  }
}
export type CustomStorage = ReturnType<typeof CustomStorage>
