import type { PG } from '@core/database/pg/wrapper'
import { table as migrationTable } from '../tables/_migrations'

export const up = async (pg: PG) => {
  const createMigrationTable = migrationTable.create().ifNotExists().toQuery()
  await pg.query(createMigrationTable)
}

export const down = async () => {
  // Not removing migration table intentionnaly as we may still need it.
}
