import type { PG } from '@core/database/pg/wrapper'
import { table as shapesTable } from '../tables/shapes'

export const up = async (pg: PG) => {
  const createShapeTable = shapesTable.create().ifNotExists().toQuery()
  await pg.query(createShapeTable)
}

export const down = async (pg: PG) => {
  const dropShapeTable = shapesTable.drop().ifExists().toQuery()
  await pg.query(dropShapeTable)
}
