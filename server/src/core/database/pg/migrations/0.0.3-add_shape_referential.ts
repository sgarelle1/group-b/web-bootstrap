import type { PG } from '@core/database/pg/wrapper'
import { makeShapeId } from '@shared/model/shape'
import { table as shapesTable, ShapeEntry } from '../tables/shapes'

const shapes: ShapeEntry[] = [
  { id: makeShapeId('1'), size: 5 },
  { id: makeShapeId('2'), height: 3, base: 4 }
]

// TODO: experiment possibility of using copy instead
export const up = async (pg: PG) => {
  const queries = shapes.map(shape => shapesTable.insert(shape).toQuery())
  await pg.transaction(async client => {
    await Promise.all(queries.map(query => client.query(query)))
  })
}

export const down = async (pg: PG) => {
  const queries = shapes.map(shape =>
    shapesTable
      .delete()
      .from(shapesTable)
      .where(shapesTable.id.equals(shape.id))
      .toQuery()
  )
  await pg.transaction(async client => {
    await Promise.all(queries.map(query => client.query(query)))
  })
}
