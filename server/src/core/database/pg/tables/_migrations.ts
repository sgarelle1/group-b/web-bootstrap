import { object, string } from 'idonttrustlikethat'
import { sql } from '@core/database/pg/wrapper'

export const migrationEntryValidator = object({
  migration: string
})

export type MigrationEntry = typeof migrationEntryValidator.T

export const table = sql.define<MigrationEntry>({
  schema: 'public',
  name: 'migration',
  columns: {
    migration: { dataType: 'varchar', primaryKey: true }
  }
})
