import { sql } from '@core/database/pg/wrapper'
import { apiShape } from '@shared/model/shape'

export const shapeEntry = apiShape
export type ShapeEntry = typeof shapeEntry.T

export const table = sql.define<ShapeEntry>({
  schema: 'public',
  name: 'shape',
  columns: {
    id: { dataType: 'varchar', primaryKey: true },
    size: { dataType: 'integer' },
    base: { dataType: 'integer' },
    height: { dataType: 'integer' }
  }
})

// Replace all keys except the primary key. (id_analyse)
export const allColumnsButPrimary = table.columns
  .map(c => c.name as any as keyof ShapeEntry)
  .filter(name => name !== table.id.name)
