import assert from 'assert'
import { number, object, string } from 'idonttrustlikethat'
import { Logger } from '@core/logger'
import { validateRow, validateRowOpt } from './utils'

describe('Postgres Database utils', () => {
  describe('validateRow', () => {
    it('should return a proper object if validation is successful', () => {
      // Given
      const validator = object({ foo: string, bar: number })
      const row = { foo: 'hello', bar: 17 }
      // When
      const result = validateRow(validator, 'test 1', row, Logger)
      // Then
      assert.deepStrictEqual(result, row)
    })

    it('should return a simpler object if row has undefined fields', () => {
      // Given
      const validator = object({ foo: string, bar: number })
      const row = { foo: 'hello', bar: 17, baz: true }
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const { baz, ...expected } = row
      // When
      const result = validateRow(validator, 'test 1', row, Logger)
      // Then
      assert.deepStrictEqual(result, expected)
    })

    it('should throw if row does not have specified fields', () => {
      // Given
      const validator = object({ foo: string, bar: number })
      const row = { foo: 'hello' }
      const name = 'test 1'

      assert.throws(
        () => validateRow(validator, name, row, Logger),
        /^Error: Could not parse/
      )
    })
  })

  describe('validateRowOpt', () => {
    it('should return a proper object if validation is successful', () => {
      // Given
      const validator = object({ foo: string, bar: number })
      const row = { foo: 'hello', bar: 17 }
      // When
      const result = validateRowOpt(validator, 'test 1', row, Logger)
      // Then
      assert.deepStrictEqual(result, row)
    })

    it('should return a simpler object if row has undefined fields', () => {
      // Given
      const validator = object({ foo: string, bar: number })
      const row = { foo: 'hello', bar: 17, baz: true }
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const { baz, ...expected } = row
      // When
      const result = validateRowOpt(validator, 'test 1', row, Logger)
      // Then
      assert.deepStrictEqual(result, expected)
    })

    it('should return undefined if row does not have specified fields', () => {
      // Given
      const validator = object({ foo: string, bar: number })
      const row = { foo: 'hello' }
      // When
      const result = validateRowOpt(validator, 'test 1', row, Logger)
      // Then
      assert.deepStrictEqual(result, undefined)
    })
  })
})
