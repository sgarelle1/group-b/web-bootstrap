import type { Logger } from 'pino'

import { Err, Ok, unknown, Validator } from 'idonttrustlikethat'

// Attempts to validate a row object returned from node-PG.
// Throws in case the row does not conform to expectations.
export function validateRow<T>(
  validator: Validator<T>,
  name: string,
  row: unknown,
  logger: Logger
): T {
  const validated = validator.validate(row)

  if (!validated.ok) {
    logger.error('PG Row parsing error %o, %s', validated.errors, row)
    throw new Error(`Could not parse ${name} row`)
  }

  return validated.value
}

export function validateRowOpt<T>(
  validator: Validator<T>,
  name: string,
  row: any,
  logger: Logger
): T | undefined {
  try {
    return validateRow(validator, name, row, logger)
  } catch (_) {
    return undefined
  }
}

export const pgDateValidator: Validator<Date> = unknown.and(value => {
  if (!(value instanceof Date)) return Err(`Value ${value} is not a date`)

  return Ok(value)
})
