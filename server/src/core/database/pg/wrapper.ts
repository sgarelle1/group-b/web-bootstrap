import type { PoolClient, QueryConfig, QueryResult, QueryResultRow } from 'pg'
import { Pool } from 'pg'
import { Sql } from 'sql-ts'
import { Logger } from '@core/logger'

export const sql = new Sql('postgres')

interface PGConfig {
  application: {
    name: string
  }
  database: {
    uri: string
    connectionTimeout: number
  }
}

const logger = Logger.child({ name: 'PG' })

/**
 * A thin wrapper over a pool of connection to a PG database.
 */

export type PG = ReturnType<typeof PG>

export function PG(config: PGConfig) {
  const pool = new Pool({
    application_name: config.application.name,
    connectionTimeoutMillis: config.database.connectionTimeout,
    connectionString: config.database.uri
  })
  if (!pool) throw new Error('DB pool initialization error : pool is undefined')

  return {
    query: async <R extends QueryResultRow = any, I extends any[] = any[]>(
      queryTextOrConfig: string | QueryConfig<I>,
      values?: I
    ): Promise<QueryResult<R>> => {
      logger.info({
        totalCount: pool.totalCount,
        waitingCount: pool.waitingCount,
        idleCount: pool.idleCount
      })
      return pool.query(queryTextOrConfig, values)
    },

    transaction: async <T>(queriesFn: (client: PoolClient) => Promise<T>): Promise<T> => {
      const client = await pool.connect()

      try {
        await client.query('BEGIN')
        const result = await queriesFn(client)
        await client.query('COMMIT')
        return result
      } catch (err) {
        await client.query('ROLLBACK')
        logger.error('DB transaction error', err)
        return Promise.reject(err)
      } finally {
        client.release()
      }
    },

    onShutdown: () => {
      return pool.end()
    }
  }
}
