export interface Repository<RESOURCE, ID> {
  create(entry: RESOURCE): Promise<RESOURCE>
  update(entry: RESOURCE): Promise<number>
  delete(id: ID): Promise<number>

  lookup(id: ID): Promise<RESOURCE | undefined>
  list(): Promise<RESOURCE[]>
}
