import dotenv, { DotenvConfigOptions } from 'dotenv'
import { object, string, union, errorDebugString } from 'idonttrustlikethat'

// Note that all raw values found in process.env are strings.
const envVariablesSchema = object({
  APP_NAME: string.optional().default('Web bootstrap'),
  API_HOST: string.optional().default('0.0.0.0'),
  API_PORT: string.optional().default(5001).map(Number),
  API_PREFIX: string.optional(),
  API_PROTOCOL: union('http', 'https').optional().default('http'),
  AUTOMATIC_MIGRATION: string
    .optional()
    .map(_ => _ === 'true')
    .default(false),
  EXTERNAL_URL: string.optional(),
  NODE_ENV: union('production', 'development', 'test'),
  SHOULD_MOCK_DATA: string
    .optional()
    .map(_ => _ === 'true')
    .default(false),
  POSTGRESQL_CONNECTION_TIMEOUT: string.optional().default(5000).map(Number),
  POSTGRESQL_URL: string,
  MONGODB_URI: string
}).map(_ => ({
  ..._,
  EXTERNAL_URL:
    _.EXTERNAL_URL === undefined
      ? `${_.API_PROTOCOL}://${_.API_HOST}:${_.API_PORT}`
      : _.EXTERNAL_URL
}))

export type EnvVariables = typeof envVariablesSchema.T

export function validateEnvVariables(options?: DotenvConfigOptions): EnvVariables {
  dotenv.config(options)
  const result = envVariablesSchema.validate(process.env)
  if (!result.ok) {
    const errors = errorDebugString(result.errors)
    throw Error(`Cannot start, missing environment variable:\n ${errors}`)
  }
  return result.value
}
