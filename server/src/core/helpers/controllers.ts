import type { FastifyReply, FastifyRequest } from 'fastify'
import type { Validator } from 'idonttrustlikethat/commonjs/validation'

import { badRequest } from '@core/api/errors'

export const getBody = <T>(
  validator: Validator<T>,
  req: FastifyRequest,
  rep: FastifyReply
): T => {
  const validated = validator.validate(req.body)
  if (!validated.ok) {
    badRequest(rep, {
      description: 'Invalid body',
      errors: validated.errors
    })
    throw new Error(`Got invalid body for request '${req.id}'`)
  }
  return validated.value
}

export const getPathParams = <T>(
  validator: Validator<T>,
  req: FastifyRequest,
  rep: FastifyReply
): T => {
  const validated = validator.validate(req.params)
  if (!validated.ok) {
    badRequest(rep, {
      description: 'Invalid path parameters',
      errors: validated.errors
    })
    throw new Error(`Got invalid path for request '${req.id}'`)
  }
  return validated.value
}
