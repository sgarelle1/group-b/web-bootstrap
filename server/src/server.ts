import { getDatabase as getDatabasePG } from '@core/database/pg/getDatabase'
import { getDatabase as getDatabaseMongo } from '@core/database/mongo/getDatabase'
import { migrate as migratePg } from '@core/database/pg/migrations'
import { migrate as migrateMongo } from '@core/database/mongo/migrations'
import { validateEnvVariables } from '@core/environment'

import { initializeWithMockedData } from '@components/shapesMongo/data/init'

import { build } from './app'

const start = async () => {
  global.env = validateEnvVariables()

  try {
    const port = global.env.API_PORT
    const pg = await getDatabasePG()
    const mongo = await getDatabaseMongo()

    if (global.env.AUTOMATIC_MIGRATION) {
      await migratePg(pg)
      await migrateMongo(mongo)
    }
    const { app, modules } = build(pg, mongo)

    if (global.env.SHOULD_MOCK_DATA) {
      // Why no initialize PG ?

      // Initialize Mongo
      await initializeWithMockedData(modules.shapeMongo.service)
    }

    await app.listen(port, global.env.API_HOST)
    console.info(`✅ server running on port ${port}`)
  } catch (err) {
    console.error(err)
    process.exit(1)
  }
}

start()
