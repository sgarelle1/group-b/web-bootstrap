import { EnvVariables } from '@core/environment'

declare global {
  namespace NodeJS {
    interface Global {
      env: EnvVariables
    }
  }
}
