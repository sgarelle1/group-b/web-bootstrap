#!/usr/bin/env bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
ROOT="$DIR/../../.."

(cd $ROOT && docker build -t ud_bootstrap_fastify:local . -f server/tooling/publish/Dockerfile)

(cd $DIR && docker-compose up)
