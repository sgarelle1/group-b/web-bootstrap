import { intersection, number, object, string, union } from 'idonttrustlikethat'

export const squareSchema = object({
  size: number
})

export const triangleSchema = object({
  base: number,
  height: number
})

export const shape = union(triangleSchema, squareSchema)

export type Shape = typeof shape.T

export type ShapeId = string & { __tag: 'ShapeId' }

export const shapeId = string.tagged<ShapeId>()

export const makeShapeId = (fromString: string): ShapeId => {
  return fromString as ShapeId
}

export const withId = object({ id: shapeId })
export const apiShape = union(
  intersection(squareSchema, withId),
  intersection(triangleSchema, withId)
)

export type ApiShape = typeof apiShape.T
