# Svelte Front bootstrap

## Prerequisites

Install `yarn` globally. All other dependencies are local.

## Running the app in dev mode

- Copy paste `.env.example` and rename it to `.env`, or set the environment variables such as `BACKEND_URL`.
- Run `yarn dev` (then it's available at [localhost:5000](http://localhost:5000))

To save on build time, static assets located in /public are only copied once at the start of the build.
This means you need to restart the dev build anytime you add / update static assets.

## Building the app for distribution in production

Run the following command to create the `dist`.

```
yarn version --new-version $NEW_VERSION --no-git-tag-version
yarn build
```

## Deploying as Docker image in production

After the distribution is built (see therebefore), then build the Docker image:

```
cp -R dist tooling/docker/dist
docker build -t svelte-frontend:$NEW_VERSION tooling/docker
```

Then the built image can be executed with the required environment variables.

    docker run -P -e BACKEND_URL=http://backend:0 svelte-frontend:$NEW_VERSION

## Configuration

The following settings can be defined, in `.env` or as environment variables, to configure this frontend.

- `BACKEND_URL` (`string`) - URL to the API/backend.
- `APP_ENV` (`string`) - Application environment, also used as Datadog `env` tag (e.g. `TST`, `STG` ...).
- `DATADOG_CLIENT_TOKEN` (optional `string`) - Datadog [client token](https://docs.datadoghq.com/account_management/api-app-keys/); If you want to use RUM (see `DATADOG_APP_ID`), do not create/ask for a separate client token for browser log, but use the client token generated along the RUM app.
- `DATADOG_SERVICE` (optional `string`) - Name of `service` tag for Datadog
- `DATADOG_TEAM` (optional `string`) - Name of `team` tag for Datadog
- `DATADOG_APP_ID` (optional `string`) - Datadog [RUM](https://docs.datadoghq.com/real_user_monitoring/browser/) application ID
- `DATADOG_SITE` (optional `string`) - Datadog site (`datadoghq.eu` or `datadoghq.com`; default: `datadoghq.eu`)
- `DATADOG_LEVEL` (optional `string`) - Logging level for Datadog (`error`, `warn`, `info`, `debug`, default: `info`).

> See the [script](./tooling/docker/envsubst-on-appenv.sh) used in Docker environment to propagate the environment variables.

## Good practices

- Never use `default` exports. Named exports are easier to import via auto-complete and are properly named by default.
- Never use `index.ts` files. These pollutes the codebase and makes searching for a given symbol more difficult.
- Never use the `any` type, unless it really helps writing an encapsulated algorithm with complex internal types.
- `eslint` is not included as it's too slow on top of `prettier` which is generally way more useful and less context dependent. Make good use of those code reviews ;-)
- Only a basic livereload is included as `Hot module replacement` is very fragile.
- Use camelCase for everything: css classes, TS code, JSON, env variables, etc.
- Don't use global JS variable/functions directly but use their `window.` form, for instance `window.setTimeout` so that the proper typings are picked up.

## Dependencies

### rollup

Way saner than webpack, better documentation. The future is bundleless development (snowpack/vite/etc) but it's not quite production ready.

### mocha

Faster and comes with less black-magic than `jest`.

### space-lift

Your one-stop shop for collection utilities and immutable updates.

### idonttrustlikethat

Helps you validate anything, but mostly JSON. Anything coming from the outside in a serialized format should be validated:
URL query/path params, data from localStorage, data from web services, etc.

### typescript-router

A simple router that is fully type-safe and work hand-in-hand with the `idonttrustlikethat` library.
