import type { RollupOptions } from 'rollup'
import fs from 'fs'
import path from 'path'
import chalk from 'chalk'
import svelte from 'rollup-plugin-svelte'
import resolve from '@rollup/plugin-node-resolve'
import commonjs from '@rollup/plugin-commonjs'
import replace from '@rollup/plugin-replace'
import alias from '@rollup/plugin-alias'
import copy from 'rollup-plugin-copy'
import esbuild from 'rollup-plugin-esbuild'
// @ts-ignore
import css from 'rollup-plugin-css-asset'
import generateFile from '@rollup/plugin-html'
// @ts-ignore
import livereload from 'rollup-plugin-livereload'
import { scss } from 'svelte-preprocess'
import { typescript as esbuildSvelteScript } from 'svelte-preprocess-esbuild'

import { config } from 'dotenv'
import { envValidator } from './envValidator'
import { errorDebugString } from 'idonttrustlikethat'
import { indexTemplate } from './indexTemplate'
import md5 from 'crypto-js/md5'
import { publicAssets, hashedPublic, generateHashedFileMap } from './assets'
import { version } from '../package.json'

const buildDirectory = path.resolve(__dirname)

const production = !process.env.ROLLUP_WATCH

const esbuildTarget = production ? 'es6' : 'es2017'

// Application environment
const appEnv = envValidator.validate({
  isProd: production,
  ...config().parsed, // dev .env file, empty in production.
  ...process.env // production.
})

if (!appEnv.ok) {
  throw new Error(
    `The app was given the wrong env variables: \n${errorDebugString(appEnv.errors)}`
  )
}

const appEnvRepr = `var appEnv = ${JSON.stringify(appEnv.value)}`
const appEnvMd5 = md5(appEnvRepr).toString()
const appEnvFileName = `app-env.${appEnvMd5}.js`

// Only once at build startup
const generatedDir = 'src/_generated'

if (!fs.existsSync(generatedDir)) {
  fs.mkdirSync(generatedDir)
}

fs.writeFileSync(`${generatedDir}/public.hash.ts`, generateHashedFileMap(hashedPublic))

const options: RollupOptions = {
  input: 'src/main.ts',

  // Note: Only generate a single chunk for now. If you want multiple chunks, watch out for this bug:
  // https://github.com/egoist/rollup-plugin-esbuild/issues/177
  output: {
    name: 'App',
    sourcemap: !production,
    format: 'iife',
    dir: 'dist',
    entryFileNames: production ? 'immutable/[name].[hash].js' : '[name].js',
    assetFileNames: production ? 'immutable/[name].[hash][extname]' : '[name][extname]',
    chunkFileNames: production ? 'chunk.[name].[hash].js' : '[name].js'
  },

  external: [],

  watch: {
    clearScreen: false
  },

  onwarn: warning => {
    // Skip certain warnings
    if (new Set(['THIS_IS_UNDEFINED', 'NON_EXISTENT_EXPORT']).has(warning?.code || ''))
      return

    console.warn(warning.message)
  },

  perf: false,

  plugins: [
    copy({
      targets: publicAssets.map(({ src, dest }) => ({
        src: `public/${src}`,
        dest: 'dist',
        rename: dest
      })),
      copyOnce: true
    }),

    replace({
      ...Object.fromEntries(
        // Update public assets URL with build hashes
        publicAssets.filter(i => i.h).map(({ src, dest }) => [src, dest])
      ),
      ApplicationVersion: version,
      'process.env.NODE_ENV': JSON.stringify(production ? 'production' : 'dev')
    }),

    resolve({ browser: true, extensions: ['.js', '.ts', '.json', '.svelte'] }),

    commonjs(),

    svelte({
      exclude: 'node_modules/**/*',
      preprocess: [
        esbuildSvelteScript({ target: esbuildTarget }),
        scss({
          renderSync: true,
          includePaths: ['./src/theme'],
          prependData: '@import "util.scss";'
        })
      ],
      compilerOptions: {
        // This is a better default. You can disable it on a per component basis when it makes sense.
        immutable: true
      }
    }),

    esbuild({
      include: /\.ts$/,
      exclude: /node_modules/,
      minify: production,
      target: esbuildTarget
    }),

    css({ name: 'bundle' }),

    alias({
      entries: [
        {
          find: 'src',
          replacement: path.resolve(buildDirectory, `../src`)
        },
        {
          find: 'shared',
          replacement: path.resolve(buildDirectory, '../../shared/src')
        }
      ]
    }),

    generateFile({
      template: ({ files }) => {
        // env file first so that the rest of the code can read its global var.
        const jsFiles = [{ fileName: appEnvFileName }, ...(files.js || [])]

        const script = jsFiles
          .map(({ fileName }) => `<script src='/${fileName}'></script>`)
          .join('\n')

        const css = (files.css || [])
          .map(({ fileName }) => `<link rel='stylesheet' href='/${fileName}'>`)
          .join('\n')

        return indexTemplate({
          version,
          script,
          css
        })
      }
    }),

    generateFile({
      fileName: appEnvFileName,
      template: () => appEnvRepr
    }),

    !production && devServer(),

    !production &&
      livereload({
        watch: 'dist',
        applyCSSLive: false
      })
  ]
}

function devServer() {
  let started = false

  return {
    writeBundle() {
      if (started) return

      started = true

      const sirvPort = 5000

      // sirv's --quiet mode is all or nothing so let's print our own message to know about the port.
      console.log(chalk.blue.bold(`Your application is ready at localhost:${sirvPort}`))

      require('child_process').spawn(
        'yarn',
        ['sirv', 'dist', '--dev', '--quiet', '--single', '--port', sirvPort],
        {
          stdio: ['ignore', 'inherit', 'inherit'],
          shell: true
        }
      )
    }
  }
}

export default options
