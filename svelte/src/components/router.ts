import { readable } from 'svelte/store'
import { Route, Router } from 'typescript-router'
import { withId } from 'shared/model/shape'

export const router = Router(
  {
    index: Route('/'),
    shapes: Route('/shapes'),
    shape: Route('/shape/:id', withId)
  },
  { onNotFound: reason => console.error(reason) }
)

export type AppRouter = typeof router
export type AppRoute = AppRouter['route']

export const route = readable(router.route, set => {
  const stop = router.onChange(() => set(router.route))
  return stop
})
