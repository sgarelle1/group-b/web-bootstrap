import { ApiShape, apiShape } from 'shared/model/shape'
import { vfetch, VFetchResult } from 'src/utils/fetch'

export type BlueData = VFetchResult<ApiShape> | undefined

export function fetchBlueData(id: string): Promise<BlueData> {
  return vfetch(`${appEnv.backendUrl}/api/shapesPg/${id}`, { ok: apiShape })
}
