import { array } from 'idonttrustlikethat'
import { ApiShape, apiShape } from 'shared/model/shape'
import { vfetch, VFetchResult } from 'src/utils/fetch'

export type Data = VFetchResult<ApiShape[]> | undefined

export function fetchData(): Promise<Data> {
  return vfetch(`${appEnv.backendUrl}/api/shapesPg`, { ok: array(apiShape) })
}
