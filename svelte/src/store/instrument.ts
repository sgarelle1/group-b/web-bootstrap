import { writable } from 'svelte/store'
import type { Logger } from '@datadog/browser-logs'

// See `src/utils/instrument.ts`
import { consoleLogger, loggerFactory } from 'src/utils/instrument'

const logger = writable<Logger>(consoleLogger)

export function onLogin(login: {
  userName: string
  userFullName: string
  sessionId: string
}) {
  logger.set(
    loggerFactory({
      id: login.userName,
      name: login.userFullName,
      sessionId: login.sessionId
    })
  )
}
